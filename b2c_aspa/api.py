import frappe
from frappe import _
from b2c_aspa.doctype.aspa_machine_meter_recording_tool.aspa_machine_meter_recording_tool import get_last_reading

import csv
import json

@frappe.whitelist()
def get_machine_info(machine):
	m = frappe.get_doc("ASPA Machine", machine)
	return m

@frappe.whitelist()
def add_allocation(engineer, call):
	a = frappe.new_doc("ASPA Call Allocation")
	a.allocated_call = call
	a.allocated_to = engineer
	a.engineer_name = frappe.db.get_value("Employee", engineer, "employee_name")
	a.allocated_on = frappe.utils.datetime.datetime.today()
	a.notification_count = 1
	a.save()

	frappe.db.commit()

	return "Allocated" if a else "Logged"

@frappe.whitelist()
def add_visit(allocation, engineer, engineer_name):
	v = frappe.new_doc("ASPA Call Visit")
	v.allocation = allocation
	v.allocated_call = frappe.db.get_value("ASPA Call Allocation", allocation, "allocated_call")
	v.engineer = engineer
	v.engineer_name = engineer_name
	v.save()

	frappe.db.commit()

	return v.name

@frappe.whitelist()
def send_reminder(allocation, engineer, engineer_name):
	return engineer_name
	#TODO: Wireup code for sending sms.

@frappe.whitelist()
def unlink_call_from_toner_request(toner_request):
	frappe.db.set_value("ASPA Toner Request", toner_request, "call", None)


def get_agreements_for_machines():

	machines_json = csv_to_json("/home/gaurav/gaurav-work/b2c xerox/machines.csv")

	out = { "cm" : [], "fs": [], "ss": [], "wr": [] }

	agreements_cm_json = csv_to_json("/home/gaurav/gaurav-work/b2c xerox/agreements_cm.csv")
	agreements_fs_json = csv_to_json("/home/gaurav/gaurav-work/b2c xerox/agreements_fs.csv")
	agreements_ss_json = csv_to_json("/home/gaurav/gaurav-work/b2c xerox/agreements_ss.csv")
	agreements_wr_json = csv_to_json("/home/gaurav/gaurav-work/b2c xerox/agreements_wr.csv")

	for machine in machines_json["data"]:
		#print machine["Name"]
		#print "Machine:", machine["Name"]
		cm_ags_for_machine = [ag for ag in agreements_cm_json["data"] if ag["MachineID"] == machine["Name"]]
		fs_ags_for_machine = [ag for ag in agreements_fs_json["data"] if ag["MachineID"] == machine["Name"]]
		ss_ags_for_machine = [ag for ag in agreements_ss_json["data"] if ag["MachineID"] == machine["Name"]]
		wr_ags_for_machine = [ag for ag in agreements_wr_json["data"] if ag["MachineID"] == machine["Name"]]
		#print "Agreements: ", len(cm_ags_for_machine), ", ", cm_ags_for_machine

		if len(cm_ags_for_machine) > 0:
		 	out["cm"] = out["cm"] + cm_ags_for_machine

		if len(fs_ags_for_machine) > 0:
			out["fs"] = out["fs"] + fs_ags_for_machine

		if len(ss_ags_for_machine) > 0:
			out["ss"] = out["ss"] + ss_ags_for_machine

		if len(wr_ags_for_machine) > 0:
			out["wr"] = out["wr"] + wr_ags_for_machine

	fieldnames = ["AggID", "MachineID", "CustID", "Customer Name", "Aggtype", "AggStDate", "AggEndDate", "SpoiltPer", "Rate1", "Rate2", "MinCharges", "AggStReading", "AggEndReading", "Ended", "UseTaxableRate", "TaxableRate1", "TaxableRate2", "spoiltCpyTyp"]
	with open('/home/gaurav/gaurav-work/b2c xerox/final_agreements_cm.csv', 'w') as csvfile:
	    writer_cm = csv.DictWriter(csvfile, fieldnames=fieldnames)
	    writer_cm.writeheader()
	    for row_cm in out["cm"]:
	    	writer_cm.writerow(row_cm)

	with open('/home/gaurav/gaurav-work/b2c xerox/final_agreements_fs.csv', 'w') as csvfile:
	    writer_fs = csv.DictWriter(csvfile, fieldnames=fieldnames)
	    writer_fs.writeheader()
	    for row_fs in out["fs"]:
	    	writer_fs.writerow(row_fs)

	with open('/home/gaurav/gaurav-work/b2c xerox/final_agreements_ss.csv', 'w') as csvfile:
	    writer_ss = csv.DictWriter(csvfile, fieldnames=fieldnames)
	    writer_ss.writeheader()
	    for row_ss in out["ss"]:
	    	writer_ss.writerow(row_ss)

	with open('/home/gaurav/gaurav-work/b2c xerox/final_agreements_wr.csv', 'w') as csvfile:
	    writer_wr = csv.DictWriter(csvfile, fieldnames=fieldnames)
	    writer_wr.writeheader()
	    for row_wr in out["wr"]:
	    	writer_wr.writerow(row_wr)

def csv_to_json(path, column_headings_row_idx=1, start_parsing_from_idx=2):

	def process_val(value):
		out = value.replace("\\", "\\\\").replace('"', '\\"').replace("\n", "")
		return out

	file_rows = []
	out_rows = []

	csv_path = path #'/home/gaurav/gaurav-work/skynpro/Tally2ERPNext/skynpro_tally_si.csv' #frappe.utils.get_site_path() + settlement_csv
	#outfile_name = '/home/gaurav/gaurav-work/skynpro/Tally2ERPNext/skynpro_tally_si_out.csv'

	#with open('/home/gaurav/Downloads/25a4cbe4397b494a_2016-12-03_2017-01-02.csv', 'rb') as csvfile:

	with open(csv_path, 'rb') as csvfile:
		rdr = csv.reader(csvfile, delimiter=str(','), quotechar=str('"'))

		for row in rdr:
			file_rows.append(row)

		final_json = {}
		json_data = final_json.setdefault("data", [])
		column_headings_row = file_rows[column_headings_row_idx]

		#Handle repeating columns
		processed_headings_row = []
		for col in column_headings_row:
			count = len([x for x in processed_headings_row if x == col])
			if count > 0:
				col = col + "_" + str(count)
			processed_headings_row.append(col)

		count = 0
		for i in xrange(start_parsing_from_idx, len(file_rows)):
			record_core = ""

			if len(file_rows[i]) == len(processed_headings_row):
				for j in range(0, len(processed_headings_row)):
					record_core += '"' +  processed_headings_row[j] + '" : "' + process_val(file_rows[i][j]) + '", '

				record_json_string = "{" + record_core[:-2] + "}"

				#print record_json_string
				#print i
				count += 1
				json_data.append(json.loads(record_json_string))

		print count
		return final_json

		#print "FINAL JSON", final_json

def add_meter_reading(machine, reading_type, reading_time, reading, meter, meter_reader, remark=None, source=None):
	for x in xrange(1,10):
		print "add_meter_reading source", source
	try:
		mr = frappe.new_doc("ASPA Meter Reading")
		mr.machine = machine
		mr.reading_type = reading_type or ""
		mr.reading_time = reading_time
		mr.reading = reading
		mr.meter = meter
		mr.meter_reader = meter_reader
		mr.remark = remark
		source_map = {"Toner Call": "ASPA Call", "Visit": "ASPA Call Visit"}
		if reading_type in source_map:
			mr.append("links",{
				"link_doctype": source_map[reading_type],
				"link_name": source
				})
		mr.insert(ignore_permissions=True)
		frappe.db.commit()
	except Exception as e:
		raise

#TODO: Confirm links and delete if unused.
# def get_meter_readings_for_machine(machine, reading_type="Patch", meter_reader=None):
# 	m = frappe.get_doc("ASPA Machine", machine)
# 	readings_by_meter = []
# 	for meter in m.machine_meters:
# 		reading = {
# 			"meter": meter.patch_meter,
# 			"readings":frappe.get_all("ASPA Meter Reading",
# 					filters={
# 						"machine": machine,
# 						"meter": meter.patch_meter,
# 						"reading_type": reading_type
# 					}, fields=["*"])
# 		}
# 		readings_by_meter.append(reading)

# 	return readings_by_meter
