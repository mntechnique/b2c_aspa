// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Call Visit', {
	onload: function(frm) {
		frm.set_df_property("sb_visit_time_details", "hidden", 1);
		frm.set_df_property("sb_break_call", "hidden", 1);
		frm.set_df_property("sb_close_call", "hidden", 1);

		if (!frm.doc.__islocal) {
			if(frm.doc.call_status_after_visit == "Broken") {
				btn_break_call_clicked(frm);
			} else if (frm.doc.call_status_after_visit == "Closed") {
				btn_close_call_clicked(frm);
			}
		}
	},
	btn_break_call: function(frm){
		btn_break_call_clicked(frm);
		clear_values(frm);
	},
	btn_close_call:  function(frm){
		btn_close_call_clicked(frm);
		clear_values(frm);
		render_readings_input_for_visit_log(frm);
	},
	time_completed: calculate_duration,
	time_arrived: calculate_duration
});

function btn_break_call_clicked(frm) {
	frm.set_df_property("sb_visit_time_details", "hidden", 0);
	frm.set_df_property("sb_break_call", "hidden", 0);
	frm.set_df_property("sb_close_call", "hidden", 1);
	frm.set_value("call_status_after_visit", "Broken");
}

function clear_values(frm) {
	if (frm.doc.call_status_after_visit == "Broken") {
		frm.set_value("time_completed", "");
		frm.set_value("visit_duration", "");
		frm.set_value("cause", "");
		frm.set_value("sub_cause", "");
		frm.set_value("action", "");
	} else if (frm.doc.call_status_after_visit == "Closed") {
		frm.set_value("call_broken_code", "");
		frm.set_value("emertel_no", "");
	}
}
function btn_close_call_clicked(frm) {
	frm.set_df_property("sb_visit_time_details", "hidden", 0);
	frm.set_df_property("sb_break_call", "hidden", 1);
	frm.set_df_property("sb_close_call", "hidden", 0);
	frm.set_value("call_status_after_visit", "Closed");
}

function calculate_duration(frm) {
	if (frm.doc.time_arrived && frm.doc.time_completed) {
		var time_arrived = moment(frm.doc.time_arrived);
		var time_completed = moment(frm.doc.time_completed);

		var d = moment.duration(time_completed.diff(time_arrived)).asMinutes();

		var mins = d % 60;
		var hrs = Math.floor(d/60);

		frm.set_value("visit_duration", hrs.toString() + ':' + mnt.pad_digits(mins, 2) + " hrs");
	}
}

cur_frm.add_fetch("engineer", "employee_name", "engineer_name");

function render_readings_input_for_visit_log(frm) {
	frappe.call({
		method: "get_meters_and_readings",
		doc: cur_frm.doc,
		callback: function(r){
			console.log("test",r);
			var wrapper = $(frm.fields_dict['meter_readings_input'].wrapper);
			wrapper.html(frappe.render_template("monthly_readings_with_input", {
					"readings_by_meter": r.message || []
				}));
			wrapper.append("<div class='row'><div class='col-xs-8'><button class='btn btn-default' id='btn-toner'>Save</button></div><div class='col-xs-8'>Current difference:</div></div>")
			console.log("btn", wrapper.find("#btn-toner"));
			//wrapper.append("<div class='panel panel-default'><table><tr>"{% for (var j=0; j < readings_by_meter.length; j+=1) { %}"<td><div>"{%= readings_by_meter[j].meter %}"</div><input class='input-with-feedback form-control input-sm ellipsis' placeholder="{%= readings_by_meter[j].meter %}" value="{%= readings_by_meter[j].previous_reading %}" disabled/></td>"{% } %}"</tr></table></div>")
			wrapper.find("#btn-toner").on("click", function(e) {
				var inputs = wrapper.find("[data-purpose='reading-input']");
				var readings_by_meter = []

				$.each(inputs, function(index, textbox) {
					reading_by_meter = {
						"meter": $(textbox).attr('data-meter'),
						"reading": $(textbox).val()
					};
					readings_by_meter.push(reading_by_meter);

				});
				console.log("TEST-", readings_by_meter);
				cur_frm.set_value('toner_readings' , JSON.stringify(readings_by_meter));
			});
		}
	});
}
