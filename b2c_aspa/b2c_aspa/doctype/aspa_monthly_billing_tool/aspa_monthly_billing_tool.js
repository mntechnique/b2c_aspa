// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Monthly Billing Tool', {
	month: function(frm) {
		get_monthly_records(frm);
	},
	refresh: function(frm) {
		frm.add_custom_button(__('Print Proforma'), function(){
			msgprint("Proforma for " + cur_frm.doc.month + " generated.");
		});
		frm.add_custom_button(__('Generate FSMA Invoices'), function(){
			msgprint("FSMA Invoices for " + cur_frm.doc.month + " generated.");
		});
	},
	validate: function(frm) {
		//Create JS and store in doctype:
		var rows = $("tr[data-purpose='machine-data-row']");
		var machine_readings = [];

		$.each(rows, function(rowindex, row) {
			var machine_reading = {};

			machine_reading["machine"] = $(row).attr("id");

			var inputs = $(row).find("[data-purpose='reading-input']");
			var readings_by_meter = {};
			$.each(inputs, function(index, textbox) {
				readings_by_meter[$(textbox).attr('data-meter')] = $(textbox).val();
			});
			machine_reading["current_readings"] = readings_by_meter;
			machine_readings.push(machine_reading);
		});

		console.log("Final readings", machine_readings);
		console.log("Onload", cur_frm.doc.__onload);
		cur_frm.doc["aspa_machine_readings"] = machine_readings;
		console.log("DOc", cur_frm.doc);
	}
});

function get_monthly_records(frm) {
	frappe.call({
		method: "get_monthly_records",
		doc: cur_frm.doc,
		callback: function(r){
			var wrapper_fsma = $(frm.fields_dict['input_readings'].wrapper);
			wrapper_fsma.html(
				frappe.render_template("monthly_billing_tool_content", {
						"all_meters": r.message.all_meters,
						"machines_info": r.message.machines_info,
					}
				));
			// wrapper_fsma.find(".add-reading").on("click", function(e) {
			// 	var inputs = wrapper_fsma.find("[data-purpose='reading-input']");
				
			// 	var readings_by_meter = {};
				
			// 	$.each(inputs, function(index, textbox) {
			// 		readings_by_meter[$(textbox).attr('class')] = $(textbox).val();
			// 	});

			// 	console.log("READINGS BY METER", readings_by_meter);

			// 	// frappe.call({
			// 	// 	method: "verify_and_add_readings",
			// 	// 	doc: cur_frm.doc
			// 	// 	args: {
			// 	// 		"readings_by_meter": readings_by_meter
			// 	// 	},
			// 	// 	callback: function(r) {
			// 	// 		if (r.message) {
			// 	// 			$.each(inputs, function(index, textbox) {
			// 	// 				$(textbox).val("");
			// 	// 			});
			// 	// 		}						
			// 	// 	}
			// 	// })
			// });
		}
	})
}
